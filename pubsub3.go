package main

import (
	"fmt"
	"net"
	"os"
)

type Client struct{
	clientName string
	Connection net.Conn
	incoming chan string
	outgoing chan string
	subscribers []*Client
}

type ClientManager struct{
	clientList []*Client
}

// func unsubscribe(clientNameToUnsubscribe string, clientManager *ClientManager, unsubscriber *Client){
// 	for index, client := range clientManager.clientList{
// 		if client.clientName == clientNameToUnsubscribe{
// 			for subindex, subscriberClient := range client.subscribers{
// 				if subscriberClient.clientName == unsubscriber.clientName{
// 					client.subscribers = append(client.subscribers[:subindex], client.subscribers[subindex+1:])
// 					break
// 				}
// 			}
// 		}
// 	}
// }

func subscribe(operation string, clientNameToSubscribe string, clientManager *ClientManager, subscriber *Client) bool{
	for i:=0; i< len(clientManager.clientList); i++{
		fmt.Printf("ClientNametoSubscriber -%v", clientNameToSubscribe)
		if clientManager.clientList[i].clientName == clientNameToSubscribe {
			fmt.Printf("ClientNametoSubscriber -%v", clientNameToSubscribe)
			clientManager.clientList[i].subscribers = append(clientManager.clientList[i].subscribers, subscriber)
			fmt.Printf("Operation- %v for %v - true \n", operation, clientNameToSubscribe)
			return true;
		}	
	}
	return false;
}

func publishMessage(message string, sender *Client, clientManager *ClientManager){
	fmt.Printf("SUbscribers are %v", sender.subscribers)
	for i:=0; i<len(sender.subscribers); i++ {
		fmt.Printf("Sending to %v \n", sender.subscribers[i].clientName)
		//sender.subscribers[i].Connection.Write([]byte(message)[0:])		
		sender.subscribers[i].incoming <- string(message)
	}
}

func handleIncomingMessage(message string, client *Client, clientManager *ClientManager){
	fmt.Printf("Inside incoming message - %v", message)
	if len([]byte(message)) > 4{
		operation := []byte(message)[0:4]
		param := []byte(message)[5:]

		if string(operation) == "SUBS"{
			op := string(operation)
			data := string(param)
			go subscribe(op, data, clientManager, client)
		// }else if string(operation) == "UNSB" {
		// 	op := string(operation)
		// 	data := string(param)
		// 	go unsubscribe(data, clientManager, client)
		}else{
			fmt.Println("Pushing message :")
			data := message
			go publishMessage(data, client, clientManager)
		}
	}
}

func handleOutgoing(newClient *Client, clientManager *ClientManager){
	var readBuffer [512]byte
	fmt.Printf("Inside handleOutgoing...\n")
	for {
		numberOfBytes, err := newClient.Connection.Read(readBuffer[0:])
		if err != nil{
			return
		}
		go handleIncomingMessage(string(readBuffer[0:numberOfBytes]), newClient, clientManager)
	}
}

func handleIncoming(newClient *Client){
	for {
		select {
			case newMsg := <- newClient.incoming:{
				fmt.Printf("New Message Received by %v: %v", newClient.clientName, string(newMsg))
				newClient.Connection.Write([]byte(newMsg)[0:])
			}
		}
	}
}

func handleConnection(conn net.Conn, clientManager *ClientManager){
	var readBuffer [512]byte
	newClient := new(Client)
	newClient.Connection = conn
	newClient.incoming = make(chan string)	
	clientManager.clientList = append(clientManager.clientList, newClient)

	conn.Write([]byte("Enter name"))	
		
	numberOfBytes, err := conn.Read(readBuffer[0:])
	if err != nil{		
		os.Exit(0)
	}		
	newClient.clientName = string(readBuffer[0:numberOfBytes])
	fmt.Printf("Client name %v \n", newClient.clientName)	
	fmt.Printf("Client Size %v bytes \n", len([]byte(newClient.clientName)))

	go handleIncoming(newClient)
	go handleOutgoing(newClient, clientManager)
}


func main(){
	service := "localhost:8799"
	
	addr, err := net.ResolveTCPAddr("tcp", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp4", addr)
	checkError(err)

	clientManager := new(ClientManager)

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Fprintf(os.Stderr, "FATAL: Connection Error %v",  err.Error())
			continue
		}

		go handleConnection(conn, clientManager)
	}

}

func checkError(err error){
	if err != nil {		
		fmt.Fprintf(os.Stderr, "FATAL: %v", err.Error())
		os.Exit(0)
	}
}