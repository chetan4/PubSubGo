package main

import (
	"fmt"
	"net"
	"os"		
)

type Client struct{
	Connection net.Conn
	Outgoing chan string
	Incoming chan string
}

type ClientManager struct{
	ClientList []Client
}

func handleBrokerFeed(clientManager *ClientManager, globalChannel chan string){
	for {
		fmt.Println(len(clientManager.ClientList))
		select{
			case newFeedMsg := <- globalChannel:{
				fmt.Printf("Msseage arrived : %v \n", string(newFeedMsg))				
				fmt.Printf("Now forwarding it: \n")
				clientList := clientManager.ClientList
				
				for i:=0; i<len(clientList); i++ {
					clientList[i].Connection.Write([]byte(newFeedMsg[0:]))
				}
			}
		}
	}
}


func handleIncoming(client Client){
	fmt.Fprintf(os.Stderr, "Display Client %T", client.Connection)
	for {		
		select{
			case newMsg := <- client.Incoming:
				//fmt.Printf("Msg: %v", string(newMsg))
				conn := client.Connection
				conn.Write([]byte(newMsg[0:]))

		}
	}
}

func handleOutgoing(client Client, globalChannel chan string){
	var readBuffer [512]byte
	for{
		numberOfBytes, err := client.Connection.Read(readBuffer[0:])
		if err != nil {
			return
		}
		//fmt.Printf("Msg: %v", string(readBuffer[0:numberOfBytes]))
		globalChannel <- string(readBuffer[0:numberOfBytes])
	}
}

func handleConnection(conn net.Conn, clientManager *ClientManager, globalChannel chan string){
	NewSendChannel := make(chan string)
	NewReceiveChannel := make(chan string)
	NewClient := Client{Connection:conn, Outgoing:NewSendChannel, Incoming:NewReceiveChannel}
	clientManager.ClientList = append(clientManager.ClientList, NewClient)	
	go handleIncoming(NewClient)	
	go handleOutgoing(NewClient, globalChannel)
}

func main(){
	service := "localhost:8799"
	globalChannel := make(chan string)

	addr, err := net.ResolveTCPAddr("tcp", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp", addr)
	checkError(err)

	clientManager := new(ClientManager)
	go handleBrokerFeed(clientManager, globalChannel)

	for {
		conn, err := listener.Accept()
		if err != nil{
			fmt.Fprintf(os.Stderr, "FATAL: Connection Error %v", err.Error())			
			continue
		}

		fmt.Printf("%T", conn)
		go handleConnection(conn, clientManager, globalChannel)
	}

}

func checkError(err error){
	if err != nil{
		fmt.Fprintf(os.Stderr, "FATAL: %v", err.Error())
	}
}
