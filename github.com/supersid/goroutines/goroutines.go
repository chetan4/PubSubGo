package main

import (
	"fmt"
	"net/http"
	"time"
	//"io"
	//"os"
)

func Fetch(url string, c chan <- string){
	start := time.Now()
	_, err := http.Get(url)
	if err != nil{															// The error handling is explict in GO. 
		fmt.Printf("Something is totally messed up - %v", err)				// Functions return 2 values at times to indicate an error condition
	}	
	
	c <- fmt.Sprintf("For %v : %v",url, time.Since(start).Seconds())		// Send message into the channel.
	
}

func main(){
	 n := 3
	 buffer :=  1
	 channel := make(chan string, buffer) 				// We create a channel here. We'll come to buffer later. Its one by default
	 startTime := time.Now();							
	 go Fetch("https://www.shipsticks.com", channel)	// Runs independently
	 go Fetch("http://timesofindia.com", channel)
	 go Fetch("http://idyllic-software.com", channel)

	for i:=0; i < n; i++ {
		fmt.Println(<- channel)								// Receive from the channel.
	}

	fmt.Printf("Total Time %v", time.Since(startTime).Seconds())
	//time.Sleep(9000 * time.Millisecond)	
}