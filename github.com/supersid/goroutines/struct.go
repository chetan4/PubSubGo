package main

import (
	"fmt"
)

type person struct{
	age int8
	name string
}

func main(){
	p := &person{28, "Siddharth"}
	fmt.Println(p)
}