package main

import (
	"fmt"
	"time"
	"math/rand"
)

func Consume(channel chan string){
	//var sum int64 = 0	
	//var number int64 = 0

	for{
		rand.Seed(time.Now().Unix())
		randomNumber := rand.Int63n(10)		
		fmt.Printf("[Producer] sleeping for %v \n", randomNumber)
		time.Sleep(time.Duration(randomNumber) * time.Second)
		fmt.Printf("To string is %v \n", string(randomNumber))
		channel <- string(randomNumber)
		
		//time.Sleep(1 * time.Second)
	}	
}

// func Produce(channel chan string){
// 	var i int64
// 	for i=0;;i++{
// 		channel <- i
// 	}
// }

func main(){
	buffer_size := 10
	channel := make(chan string, buffer_size)	
	go Consume(channel)
	for{		
		time.Sleep(3 * time.Second)		
		randomNumber := <- channel
		fmt.Printf("[Consumer] Received: %v \n", randomNumber)
	}
	select{}
	//time.Sleep(10 * time.Second)
}