package main

import (
	"fmt"
)

type Person struct {
	name string
	age int64
}

func(p *Person) Change(){
	p.age = 99
}

func main(){
	p1 := new(Person)
	p1.name = "Siddharth"
	p1.age = 28

	p1.Change()
	fmt.Println(p1)
	fmt.Println(&p1)
	fmt.Println(p1)
}