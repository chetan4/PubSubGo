package main

import (
	"fmt"
	"time"
)

func main(){
	sid := ChannelFactory()
	piu := ChannelFactory()

	for i:=0;i<=10;i++{
		fmt.Printf("Sid received: %v \n" , <- sid)		
		fmt.Printf("Piu received: %v \n", <- piu)
		time.Sleep(1 * time.Second)
	}


}

func spam(spamChannel chan int8){
	for i:=0;i<=10;i++ {
		spamChannel <- int8(i)
	}
}

func ChannelFactory() chan int8{
	newChannel := make(chan int8)
	go spam(newChannel)
	return newChannel
}