package main

import (
	"fmt"
)

type Request struct{
	a, b int
	replyc chan int // reply channel inside the request
}

type binOp func(a, b int) int

func run(op binOp, req *Request){
	req.replyc <- op(req.a, req.b)
}

func server(op binOp, service chan *Request){
	fmt.Println("Received = Server - looping...")
	for {
		req := <- service
		go run(op, req)
	}
}

func startServer(op binOp) chan *Request{
	reqChan := make(chan *Request)
	fmt.Println("Received = startServer - sending to server")
	go server(op, reqChan)
	return reqChan
}

func main(){
	adder :=  startServer(func(a, b int) int {return a + b})
	fmt.Println("============")
	fmt.Println(adder)
	fmt.Println("============")
	const N = 10
	var reqs [N]Request

	for i:=0; i<N; i++{
		req := &reqs[i]
		req.a = i 
		req.b = i + 1
		req.replyc = make(chan int)
		adder <- req		
	}

	for j:=0; j<N; j++ {
		returnedValue := <- reqs[j].replyc
		fmt.Printf("%v ,", returnedValue)
	}
}