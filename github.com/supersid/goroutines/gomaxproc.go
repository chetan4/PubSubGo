package main

import (
	"fmt"
	"time"
	"runtime"
)

func CallThisFunc(){
	time.Sleep(3 * time.Second)
	fmt.Printf("Done Sleeping in CallThisFunc \n")
}

func CallThatFunc(){
	time.Sleep(3 * time.Second)
	fmt.Printf("Done Sleeping in CallThatFunc \n")
}

func main(){	
	runtime.GOMAXPROCS(2)
	start := time.Now()
	go CallThisFunc()
	go CallThatFunc()
	end := time.Since(start).Seconds()
	fmt.Printf("Finished in %v \n", end)
	time.Sleep(10 * time.Second)
}


