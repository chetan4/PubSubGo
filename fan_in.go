package main

import (
	"fmt"
	"chan_factory"
)

func fanIn(sid, piu chan string) <-chan string{
	fannedIn := chan_factory.NewStringChannel();
	go func(){
		for {fannedIn <- <-sid}
	}()

	go func(){
		for {fannedIn <- <-piu}
	}()
	return fannedIn
}

func main(){
	fmt.Println("Testing")
	sid := chan_factory.NewStringChannel()
	piu := chan_factory.NewStringChannel()
	fannedIn := fanIn(sid, piu)
	
	go spam(sid, piu)
	for{
		select{
			case someText := (<- fannedIn):
				fmt.Println("Data received: " + someText)
		}
	}

	
}

func spam(chan1, chan2 chan string){	
	for i:=0;;i++ {
		chan1 <- fmt.Sprintf("ALoha %v", i)
		chan1 <- fmt.Sprintf("ALoha %v", i + 22)
	}
}