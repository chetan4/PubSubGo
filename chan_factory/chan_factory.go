package chan_factory

// import(
// 	"fmt"
// )

func NewInt64Channel() chan int64{
	newChannel := make(chan int64)
	return newChannel
}

func NewStringChannel() chan string{
	newStringChannel := make(chan string)
	return  newStringChannel
}
