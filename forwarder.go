package main

import (
	"fmt"
	"net"
	"os"
)

func handleOperation(conn net.Conn, readBuffer []byte, publisher chan string){
	operation := string(readBuffer[0:4])
	message := string(readBuffer[4:])

	fmt.Printf("Operations is %v \n", string(operation))
	fmt.Printf("Message is %v \n", string(message))
	if operation == "SUBS" {		
		go func(){
			fmt.Println("In Side SUBS")
			for{
				data := <- publisher
				conn.Write([]byte(data))
			}
			}()		
	} else if operation == "PUBL" {
		go func(){			
			publisher <- message
		}()
	} else {
		conn.Write([]byte("Good Bye"))
		conn.Close()
	}

}

func handleConnection(conn net.Conn, publisher chan string){
	var readBuffer [512]byte
	

	for {
		n, err := conn.Read(readBuffer[0:])
		if err != nil{
			return
		}

		if n >= 4 {
		 	operation := string(readBuffer[0:n])	
		 	fmt.Printf("Data is operations %v \n", operation)
		 	go handleOperation(conn, readBuffer[0:], publisher)
		}			
	}
}

func main(){
	publisher := make(chan string)
	service := "localhost:8799"

	addr, err := net.ResolveTCPAddr("tcp", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp4", addr)
	checkError(err)

	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}

		go handleConnection(conn, publisher)
		
	}

}

func checkError(err error){
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal Error %s \n", err.Error())
		os.Exit(1)
	}
}

