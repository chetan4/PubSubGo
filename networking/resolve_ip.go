package main

import (
	"net"
	"fmt"
	"os"
)

func main(){
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "Usage: %s Hostname", os.Args[0])
		os.Exit(0)
	}

	name := os.Args[1]
	addr, err := net.ResolveIPAddr("ip", name)

	if err != nil{
		fmt.Println("Ip Resolution Error", err.Error())
		os.Exit(0)
	}

	fmt.Println("Resolved Address is ", addr.String())

}