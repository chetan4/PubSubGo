package main

import (
	"fmt"
	"os"
	"net"	
//	"io/ioutil"
//	"time"
)

func main(){
	service := "127.0.0.1:8799"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	for {
		fmt.Println("Waiting for next connection")
		conn, err := listener.Accept()
		if err != nil {
			continue
		}

		fmt.Println("Connection Accepted: ", conn )
		readBuffer := make([]byte, 1024)
		//readHeader := make([]byte, 8)
		for {
			nr, err := conn.Read(readBuffer)

			if err != nil {
				return	
			}

			fmt.Printf("Read Buffer is %v", string(readBuffer[0:nr]))			
		}
		data := string(readBuffer[0:])
		//data, err := conn.Read(readBuffer)
		//daytime := time.Now().String()
		
		fmt.Println("Data was :", data)
		conn.Write([]byte(readBuffer))
		conn.Close();
	}
}

func checkError(err error){
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal Error: %s \n", err.Error())
	}
}