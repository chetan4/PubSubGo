package main

import (
	"net"
	"fmt"
	"os"
)

func main(){
	if len(os.Args) != 3 {
		fmt.Fprintf(os.Stderr, "Usage: %s network-type Service", os.Args[0])
		os.Exit(0)
	}
	
	port, err := net.LookupPort(os.Args[1], os.Args[2])

	if err != nil{
		fmt.Println("Ip Resolution Error", err.Error())
		os.Exit(0)
	}

	fmt.Println("Port Address is ", port)

}