package main

import (
	"fmt"  // single quotes and double quotes are NOT the same
		   // single quotes tries to evalutate the unicode value
)

// Like C is where your code begins
func main(){
	// Data type after the variable name
	var count int

	count = 33

	fmt.Printf("Count is: %v  \n", count)

}

