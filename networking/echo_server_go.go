package main

import (
	"fmt"
	"net"
	"os"
)

func handleConnection(conn net.Conn){
	var readBuffer [512]byte
	for{
		n, err := conn.Read(readBuffer[0:])
		if err != nil{
			return
		}

		//fmt.Println(string(readBuffer[0:n]))
		conn.Write(readBuffer[0:n])		
		conn.Close()
	}
}

func main(){
	service := "localhost:8799"

	addr, err := net.ResolveTCPAddr("tcp", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp4", addr)
	checkError(err)

	for {
		conn, err := listener.Accept()
		if err != nil{
			continue
		}
		go handleConnection(conn)
		conn.Close()
	}	
}

func checkError(err error){
	if err != nil{
		fmt.Fprintf(os.Stderr, "Fatal Error: %s", err.Error())
		os.Exit(1)
	}
}