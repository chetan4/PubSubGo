package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "Usage: %s dotted-ip-address \n", os.Args[0])
		os.Exit(1)
	}

	//type IP []byte

	dotAddr := os.Args[1]
	addr := net.ParseIP(dotAddr)

	if addr == nil {
		fmt.Println("Invalid Address")
		os.Exit(1)
	}

	// addr is of type IP
	mask := addr.DefaultMask();
	// Returns the network of that IP Address
	network := addr.Mask(mask)
	//ones, bits := mask.Size()

	fmt.Println("Address is ", addr.String(), 
		"Default Mask is ", mask,
		"Network is ", network.String())
}