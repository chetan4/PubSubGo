package main

import (
	"fmt"
	"time"
)

func printEvens(){
	for i:=0;;i=i+2 {
		fmt.Printf("Running in a go routines: %v \n", i)
		time.Sleep(1 * time.Second)
	}
}

func main() {
	go printEvens()											//The go (goroutine) forces to the code to run asynchronously
															//This is not a Thread but a lightweight thread.	
															//Remove the go and the main thread will never execute.
	for i:= 0; i<100 ; i++ {
		fmt.Printf("Running in the main thread: %v \n", i)
		time.Sleep(2 * time.Second)
	}
}