package main

import (
	"fmt"   // fmt is the data formating library
			// single quotes and double quotes are NOT the same
		    // single quotes tries to evalutate the unicode value
)

// Like C is where your code begins
func main(){
	// Data type after the variable name
	var count int
	count = 33

	// Type inference. If you don't like defining the type
	inferCount := 34

	fmt.Printf("Count is: %v \n", count)
	fmt.Printf("Infered count is %v \n", inferCount)

}

