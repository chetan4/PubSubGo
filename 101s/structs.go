package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func (v *Vertex) Abs() float64 {    		// Can call the Abs function on a Vertex struct
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex{1, 2}
	v.X = 4
	fmt.Println(v.X)

	p := Vertex{1, 2}
	q := &p 			//Pointers here. Assign a reference to Vertex p
	q.X = 4				// Changing q affects p as they are references
	fmt.Println(p)		// Print out the references

	fmt.Println(q.Abs()) // Calling a method on Vertex
}