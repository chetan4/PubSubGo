package main

import (
	"fmt"   // fmt is the data formating library
			// single quotes and double quotes are NOT the same
		    // single quotes tries to evalutate the unicode value
)

// Like C is where your code begins
func main(){
	// Data type after the variable name
	var count int
	count = 33

	// Type inference. If you don't like defining the type
	inferCount := 34

	fmt.Printf("Count is: %v \n", count)
	fmt.Printf("Infered count is %v \n", inferCount)

	var x, y int = 1, 2
	fmt.Printf("X and Y are %v, %v \n", x, y)


	// Arrays
	var names [4]string  		// A array of size 4
								// Not allowed to grow beyond 4
	names[0] = "Homeland"
	names[1] = "Dexter"
	names[2] = "An Idiot Abroad"
	names[3] = "Two and Half Men"
								//names[4] = "The Big Bang Theory"  # Will Not Work. 
								//								    # Index out of bounds
	var i int
	for index, name := range names {
		i = index + 1
		fmt.Printf("%v). %v \n", i, name)
	}


	arrayLiteral := [4]string{"Mumford & Sons", "Bob Dylan", "Donovan"}
							// Array Literal assignment. Still Fixed length arrays.
	for index, artist := range arrayLiteral{
		i = index + 1
		fmt.Printf("%v). %v \n", i, artist)	
	}
}

