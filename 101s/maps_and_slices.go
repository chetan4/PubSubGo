package main

import (
	"fmt"   // fmt is the data formating library
			// single quotes and double quotes are NOT the same
		    // single quotes tries to evalutate the unicode value
)

// Like C is where your code begins
func main(){
	// Data type after the variable name
	var count int
	count = 33

	// Type inference. If you don't like defining the type
	inferCount := 34

	fmt.Printf("Count is: %v \n", count)
	fmt.Printf("Infered count is %v \n", inferCount)

	var x, y int = 1, 2
	fmt.Printf("X and Y are %v, %v \n", x, y)


	// Arrays
	var names [4]string  		// A array of size 4
								// Not allowed to grow beyond 4
	names[0] = "Homeland"
	names[1] = "Dexter"
	names[2] = "An Idiot Abroad"
	names[3] = "Two and Half Men"
								//names[4] = "The Big Bang Theory"  # Will Not Work. 
								//								    # Index out of bounds
	var i int
	for index, name := range names {
		i = index + 1
		fmt.Printf("%v). %v \n", i, name)
	}


	arrayLiteral := [4]string{"Mumford & Sons", "Bob Dylan", "Donovan"}
							// Array Literal assignment. Still Fixed length arrays.
	for index, artist := range arrayLiteral{
		i = index + 1
		fmt.Printf("%v). %v \n", i, artist)	
	}

	// Create a Slice
	sliceOfArtists := arrayLiteral[0:]
	sliceLiteral := []string{"Dark Passenger", "Slice of Life", "Dexter tidbits"}

	fmt.Printf("Capcity of sliceOfArtists is %v \n", cap(sliceOfArtists)) // The capacity is the number of elements in the underlying array	
	fmt.Printf("Length of sliceOfArtists is %v \n", len(sliceOfArtists))  // The length is the number of elements the slice refers to in the underlying array

	for _, dex_bits := range sliceLiteral {
		fmt.Println(dex_bits)
	}

	// Increase the size of a slice
	biggerSlice := append(sliceLiteral, "Born Free")

	fmt.Println("====================================")
	for _, dex_bits := range biggerSlice {
		fmt.Println(dex_bits)
	}


	var hash  = make(map[string]string) // Returns a reference. Works only for slices, maps and channels
										// For others there is new

	hash["name"] = "Siddharth"
	hash["occupation"] = "Day Dreamer"
	hash["passion"] = "Pizza eater"

	for key, val := range hash {
		fmt.Printf("%v: %v \n", key, val)
	}

	var hashLiteral = map[string]string{"company": "Idyllic Software", "Fearless leaders":"Jinesh & Gaurav"}
	
	for key, val := range hashLiteral {
		fmt.Printf("%v: %v \n", key, val)
	}


}


















